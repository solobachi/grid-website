// toggle menu function
// Carousel script
$(document).ready(function(){
 $(".AdsCarousel").owlCarousel({
  margin:20,
  loop:true,
  autoplay:true,
  autoplayTimeout:3000,
  autoplayHoverPause:false,
  nav:true,
  dots:false,
  responsive:{
  0:{
  items:1,
  },
  810:{
  items:2,
  },
  1200:{
  items:3,
  }
  }
 })
});


// Accordion script:
const accordionHeader = document.querySelectorAll('.accordion-header');
// console.log(accordionHeader)
for(let accordion of accordionHeader){
accordion.addEventListener("click",function(e){
e.target.classList.toggle('active');
})
}


//script to disable the downloading behaviour
const pdfFiles = document.querySelectorAll(`a[href$="pdf"]`);
// console.log(pdfFiles)
for(let file of pdfFiles){
file.addEventListener("contextmenu",(e)=>{
e.preventDefault();
})
}