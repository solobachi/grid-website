const barBtn = document.querySelector(".bar-btn");

//Listen to click event
barBtn.addEventListener("click",function(e){

//target the nav-menu
 const navMenu = document.querySelector(".nav-menu");

 const openMenu = document.querySelector(".open");

 navMenu.classList.add("open");
 e.target.classList.add("close");
 e.target.innerHTML = "&#10006";

 //close the nav-menu
 if(openMenu){
 navMenu.classList.remove("open");
 e.target.innerHTML = "&#9776";
 e.target.classList.remove("close");
 }

})


//Show Drop down Menu
const rightArrow = document.querySelector(".fa-angle-right");
rightArrow.addEventListener('click',function(e){
//get the element with dropdown-menu class
const dropDownMenu = document.querySelector(".dropdown-menu");
 dropDownMenu.classList.toggle("show-dropdown");
 e.target.classList.toggle("rotate-icon");
})